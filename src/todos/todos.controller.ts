import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { CreateTodoDto } from './dto/create-todo.dto';
import { Todo } from './interfaces/todo.interface';
import { TodosService } from './todos.service';

//localhost:300/todos
@Controller('todos')
export class TodosController {
  constructor(private readonly todoService: TodosService) {}
  //** GET BY ID /todos/:id
  @Get(':id')
  findOne(@Param('id') id: string) {
    console.log('id: ', id);
    return this.todoService.findOne(id);
  }
  //** GET ALL /todos
  @Get()
  findAll(): Todo[] {
    return this.todoService.findAll();
  }
  //** Create /todos
  @Post()
  createTodo(@Body() newTodo: CreateTodoDto) {
    console.log('newTodo = ', newTodo);
    this.todoService.create(newTodo);
  }
  //** Update /todos/id
  @Patch(':id')
  updateTodo(@Param('id') id: string, @Body() todo: CreateTodoDto) {
    return this.todoService.updateTodo(id, todo);
  }
  //** DELETE /todos/:id
  @Delete(':id')
  deleteTodo(@Param('id') id: string) {
    return this.todoService.deleteTodo(id);
  }
}
