import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTodoDto } from './dto/create-todo.dto';
import { Todo } from './interfaces/todo.interface';

@Injectable()
export class TodosService {
  // Ma BDD
  todos: Todo[] = [
    {
      id: 1,
      title: 'Création',
      description: 'Création de la todoapp NestJs',
      done: false,
    },
    { id: 2, title: 'Pain', description: 'Aller acheter du pain', done: true },
  ];
  // ** GET BY ID
  findOne(id: string) {
    return this.todos.find((t) => t.id === Number(id));
  }
  // ** GET ALL Todos
  findAll(): Todo[] {
    return this.todos;
  }
  // ** Creation d'un todo
  create(todo: CreateTodoDto) {
    this.todos = [...this.todos, todo]; //ajoute notre request à notre tableau de todo
  }
  // ** Update un todo
  updateTodo(id: string, todo: Todo) {
    const todoToUpdate = this.todos.find((t) => t.id === +id);
    if (!todoToUpdate) {
      return new NotFoundException("L'id n'existe pas !");
    }
    //Si il y a une propriété done -> modification de cette propriété
    if (todo.hasOwnProperty('done')) {
      todoToUpdate.done = todo.done;
    }
    if (todo.title) {
      todoToUpdate.title = todo.title;
    }
    if (todo.description) {
      todoToUpdate.description = todo.description;
    }
    //Si l'ID est différent , on garde les ancienne données sinon on update
    const updatedTodo = this.todos.map((t) =>
      t.id !== +id ? todo : todoToUpdate,
    );
    this.todos = [...updatedTodo];
    return { updatedTodo: 1, todo: todoToUpdate };
  }
  //** Delete un todo
  deleteTodo(id: string) {
    const nbOfTodosBeforeDelete = this.todos.length;
    this.todos = [...this.todos.filter((t) => t.id !== +id)];
    if (this.todos.length < nbOfTodosBeforeDelete) {
      return { deleteTodos: 1, nbTodos: this.todos.length };
    } else {
      return { deleteTodos: 0, nbTodos: this.todos.length };
    }
  }
}
